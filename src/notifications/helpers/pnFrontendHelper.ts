export const CONDITION_LIST = ['==', '!=', '>', '>=', '<', '<=', 'contains', '!contains', 'regex']

export const OPTION_NAMES = {
  '==': '= (equals)',
  '!=': '(!=) different from',
  '>': '> (greater than)',
  '>=': '>= (greater than or or equal to)',
  '<': '< (less than)',
  '<=': '<= (less than or or equal to)',
  contains: 'List contains',
  '!contains': "List doesn't contain",
  regex: 'regex'
}

export const DEFAULT_ACTIVE_SUBS = [
  // Core
  'transfer',
  //'limit_order',
  'fill_order',
  //'convert',
  'fill_convert_request',
  'fill_recurrent_transfer',
  'request_account_recovery',
  'producer_reward',
  'author_reward',
  'curation_reward',
  'comment_payout_update',
  // Splinterlands
  'sm_token_transfer',
  'sm_market_purchase',
  'sm_unlock_assets'
]

export const PREFIX_MAP = {
  core: '',
  splinterlands: 'sm_'
}

export interface IConfigItem {
  operation: string
  conditions: any
}

interface IConditionParams<T> {
  default?: T
  enabled?: boolean
  options?: string[]
  defaultOption?: string
  range?: T[]
  value?: T
  selectedOption?: string
}

interface ICondition<T> extends IConditionParams<T> {
  type: string
}

/**
 * Returns a new condition of the specified type
 * @param params
 * @returns
 */
const nc = (type: Function, params: IConditionParams<any> = {}) => {
  const typeVariable = type()
  let condition: ICondition<typeof typeVariable>

  condition = { type: Array.isArray(typeVariable) ? 'array' : typeof typeVariable, ...params }

  if (condition.default != undefined) {
    condition.value = condition.default
  }

  if (!condition.options) {
    condition.options = defaultOptions(condition.type)
    condition.selectedOption = condition.options[0]
  }

  if (condition.defaultOption != undefined) {
    condition.selectedOption = condition.defaultOption
  }

  return condition
}

/**
 * Returns an object with many default conditions of the same type
 * @param keys Names of the conditions to generate
 */
const mnc = (type: Function, keys: string[]) => {
  let conditions: any = {}
  for (const key of keys) {
    conditions[key] = nc(type)
  }
  return conditions
}

const defaultOptions = (type: string) => {
  const options = ['==', '!=']
  switch (type) {
    case 'string':
      return [...options, 'regex']
    case 'number':
      return [...options, '>', '>=', '<', '<=']
    case 'array':
      return ['contains', '!contains']
    default:
      return [...options, '>', '>=', '<', '<=', 'contains', 'regex']
  }
}

export const generateTemplates = (username: string) => {
  return {
    // *** HIVE CORE ***
    core: {
      settings: {
        name: 'Core',
        prefix: ''
      },
      operations: [
        {
          opType: 'vote',
          template: {
            voter: nc(String),
            author: nc(String, { default: username }),
            permlink: nc(String),
            weight: nc(Number, { range: [0, 10000] })
          }
        },
        {
          opType: 'comment',
          template: {
            author: nc(String, { default: username }),
            ...mnc(String, ['parent_author', 'parent_permlink', 'permlink', 'title', 'body', 'json_metadata'])
          }
        },
        {
          opType: 'transfer',
          template: {
            from: nc(String),
            to: nc(String, { default: username }),
            amount: nc(String),
            memo: nc(String)
          }
        },
        {
          opType: 'fill_recurrent_transfer',
          template: {
            from: nc(String),
            to: nc(String, { default: username }),
            amount: nc(String),
            memo: nc(String),
            remaining_executions: nc(Number)
          }
        },
        /*{
          opType: 'limit_order',
          template: {}
        },*/
        {
          opType: 'fill_order',
          template: {
            open_owner: nc(String, { default: username }),
            ...mnc(String, ['open_pays', 'current_owner', 'current_pays']),
            ...mnc(Number, ['open_orderid', 'current_orderid'])
          }
        },
        /*{
          opType: 'convert',
          template: {
            owner: nc(String, { default: username }),
            requestid: nc(Number),
            amount: nc(String)
          }
        },*/
        {
          opType: 'fill_convert_request',
          template: {
            owner: nc(String, { default: username }),
            requestid: nc(Number),
            amount_in: nc(String),
            amount_out: nc(String)
          }
        },

        {
          opType: 'request_account_recovery',
          template: {}
        },
        /*{
          opType: 'claim_reward_balance',
          template: {
            account: nc(String, { default: username }),
            ...mnc(String, ['reward_hive', 'reward_hbd', 'reward_vests'])
          }
        },*/
        {
          opType: 'producer_reward',
          template: {
            producer: nc(String, { default: username }),
            vesting_shares: nc(String)
          }
        },
        {
          opType: 'author_reward',
          template: {
            author: nc(String, { default: username }),
            ...mnc(String, ['permlink', 'hbd_payout', 'hive_payout', 'vesting_payout']),
            payout_must_be_claimed: nc(Boolean)
          }
        },
        {
          opType: 'curation_reward',
          template: {
            curator: nc(String, { default: username }),
            ...mnc(String, ['reward', 'comment_author', 'comment_permlink']),
            payout_must_be_claimed: nc(Boolean)
          }
        },
        {
          opType: 'comment_payout_update',
          template: {
            author: nc(String, { default: username }),
            permlink: nc(String)
          }
        }
      ]
    },
    // *** SPLINTERLANDS ***
    splinterlands: {
      settings: {
        name: 'Splinterlands',
        prefix: 'sm_'
      },
      operations: [
        {
          opType: 'custom_json',
          template: {
            required_auths: nc(Array<String>, { default: username, defaultOption: '!contains' }),
            required_posting_auths: nc(Array<String>, { default: username, defaultOption: '!contains' }),
            id: nc(String, { default: 'sm_token_transfer', enabled: false }),
            json: {
              to: nc(String),
              qty: nc(Number),
              token: nc(String),
              app: nc(String)
            }
          }
        },
        {
          opType: 'custom_json',
          template: {
            required_auths: nc(Array<String>, { default: username, defaultOption: '!contains' }),
            required_posting_auths: nc(Array<String>),
            id: nc(String, { default: 'sm_market_purchase', enabled: false }),
            json: {
              items: nc(Array<String>),
              currency: nc(String),
              price: nc(Number),
              app: nc(String)
            }
          }
        },
        {
          opType: 'custom_json',
          template: {
            required_auths: nc(Array<String>, { default: username, defaultOption: '!contains' }),
            required_posting_auths: nc(Array<String>, { default: username, defaultOption: '!contains' }),
            id: nc(String, { default: 'sm_market_rent', enabled: false }),
            json: {
              items: nc(Array<String>),
              currency: nc(String),
              days: nc(Number),
              app: nc(String)
            }
          }
        },
        {
          opType: 'custom_json',
          template: {
            required_auths: nc(Array<String>, { default: username }),
            required_posting_auths: nc(Array<String>),
            id: nc(String, { default: 'sm_lock_assets', enabled: false }),
            json: {
              lock_days: nc(Number),
              asset_ids: nc(Array<String>),
              app: nc(String)
            }
          }
        },
        {
          opType: 'custom_json',
          template: {
            required_auths: nc(Array<String>, { default: username }),
            required_posting_auths: nc(Array<String>),
            id: nc(String, { default: 'sm_unlock_assets', enabled: false }),
            json: {
              asset_ids: nc(Array<String>),
              app: nc(String)
            }
          }
        }
        /*{
          opType: 'custom_json',
          template: {
            required_auths: nc(Array<String>, { default: username }),
            required_posting_auths: nc(Array<String>),
            id: nc(String, { default: 'sm_claim_rewards', enabled: false }),
            json: {
              app: nc(String)
            }
          }
        }*/
      ]
    }
  }
}

/**
 * Populate the default values of the templates using config
 * @param templates
 * @param config
 * @returns
 */
export const populateTemplates = (templates: any, config: Array<{ operation: string; conditions: any }>): void => {
  for (const module in templates) {
    for (const operation of templates[module].operations) {
      for (const configItem of config) {
        // Skip if different operation or, if custom_json, skip for different id
        if (!isSameOperation(configItem, operation)) {
          continue
        }
        operation['active'] = true
        // Iterate through each condition and set the default value and option in the template
        for (const key in operation.template) {
          for (const conditionKey in configItem.conditions) {
            setTempalteDefaults(configItem.conditions, conditionKey, operation.template)
          }
        }
      }
    }
  }
  return templates
}

/**
 * Sets the default value and the default option in the template for all the fields found in conditionKey.
 * @param conditions conditions object found in configItem
 * @param conditionKey condition key
 * @param template template
 */
const setTempalteDefaults = (conditions: any, conditionKey: string, template: any): void => {
  const condition = conditions[conditionKey]
  const firstKey = Object.keys(condition)[0]

  // If the first key is a condition (e.g. '==') we found the value
  if (isCondition(condition)) {
    template[conditionKey].default = condition[firstKey]
    template[conditionKey].value = condition[firstKey]
    template[conditionKey].defaultOption = firstKey
    template[conditionKey].selectedOption = firstKey
    // Recurvise step
  } else if (typeof condition == 'object') {
    const newKeys = Object.keys(conditions[conditionKey])
    for (const newKey in conditions[conditionKey]) {
      setTempalteDefaults(conditions[conditionKey], newKey, template[conditionKey])
    }
  }
}

/**
 * True for objects with 1 key that is contained in CONDITIONS_LIST.
 * @param obj
 * @returns
 */
const isCondition = (obj: any): boolean => {
  if (typeof obj != 'object') {
    return false
  }
  const keys = Object.keys(obj)
  return keys.length == 1 && CONDITION_LIST.some(el => keys[0] == el)
}

/**
 * Generates an object that maps the operations in the templates to a unique hash
 * which is the operation name for 'core' and the custom_json id for other modules.
 * @param templates
 * @returns
 */
export const indexTemplates = (templates: any) => {
  let indexedTemplates: any = {}
  for (const module in templates) {
    for (const operation of templates[module].operations) {
      let hash = ''
      if (operation.opType == 'custom_json') {
        hash = operation.template.id.default
      } else {
        hash = operation.opType
      }
      indexedTemplates[hash] = operation
    }
  }
  return indexedTemplates
}

/**
 * Checks if value comforms with the definition of the condition in the template
 * @param tplCondition template condition (e.g. template.core.operations[0])
 * @param value input value
 * @param conditionName name of the condition (e.g. vote)
 * @returns
 */
export const validateTemplateElement = (tplCondition: ICondition<any>, value: any, conditionName?: string): { success: boolean; error?: string } => {
  if (tplCondition.type == 'array') {
    if (Array.isArray(value)) {
      return {
        success: true
      }
    }
    return {
      success: false,
      error: `Value ${conditionName ? `of ${conditionName}` : ''} must be made of elements separated by a comma (e.g. 1,2,3) but got ${value}`
    }
  }
  if (typeof value != tplCondition.type) {
    return {
      success: false,
      error: `Value ${conditionName ? `of ${conditionName}` : ''} must be of type ${tplCondition.type} but got ${typeof value}`
    }
  }
  if (tplCondition.range && (value < tplCondition.range[0] || value > tplCondition.range[1])) {
    return {
      success: false,
      error: `Value ${conditionName ? `of ${conditionName}` : ''} must be in range [${tplCondition.range[0]}, ${tplCondition.range[1]}] but got ${value}`
    }
  }
  if (tplCondition.default && tplCondition.enabled === false && value != tplCondition.default) {
    return {
      success: false,
      error: `Value ${conditionName ? `of ${conditionName}` : ''} must be ${tplCondition.default} but got ${value}`
    }
  }

  return {
    success: true
  }
}

/**
 * Check if configItem and template operation refer to the same type of operation.
 * If the input is a custom_json, return true if they have the same id
 * @param configItem
 * @param operation
 * @returns
 */
const isSameOperation = (configItem: { operation: string; conditions: any }, operation: any): boolean => {
  if (operation.opType == configItem.operation && configItem.operation != 'custom_json') {
    return true
  }

  if (
    configItem.operation == 'custom_json' &&
    configItem.conditions &&
    configItem.conditions.id &&
    operation.template &&
    operation.template.id &&
    operation.template.id.default &&
    operation.template.id.default == configItem.conditions.id['==']
  ) {
    return true
  }

  return false
}

export const buildUpdateNotificationSubsJson = (templates: any, config: Array<IConfigItem>): { json: string; error?: string } => {
  let json: [string, any[]] = ['update_subscriptions', []]

  for (const module in templates) {
    for (const operation of templates[module].operations) {
      let oldItem: any = {}
      let newItem: any = {}
      const configFromOp = configItemFromOperation(operation)
      if (configFromOp.error == undefined) {
        newItem = configFromOp.item
      } else {
        return { json: '', error: configFromOp.error }
      }

      // Find items in config exactly equals to newItem
      const extactMatches = findByMatching(newItem, config, matchesExactly)
      if (extactMatches.length > 0) {
        // [Delete] if inactive
        if (!operation.active) {
          json[1].push({
            old: extactMatches[0],
            new: {}
          })
        }
        continue
      }

      // Find items in config with same operation type as newItem
      const matches = findByMatching({ operation: operation.opType }, config, matchesProperties, true)
      // [Overwrite] if there was only 1 item with the same operation then overwrite it
      if (matches.length == 1) {
        oldItem = matches[0]
      }
      // If the operation is now inactive
      if (!operation.active) {
        // If isn't even in config, skip
        if (matches.length == 0) {
          continue
        }
        // [Delete] if there was an operation of this type
        newItem = {}
      }

      if (Object.keys(oldItem).length == 0 && Object.keys(newItem).length == 0) {
        continue
      }
      // Fill notify custom_json
      json[1].push({
        old: oldItem,
        new: newItem
      })
    }
  }

  // Nothing to update
  if (json[1].length == 0) {
    return { json: '' }
  }

  return { json: JSON.stringify(json) }

  /*window.hive_keychain.requestCustomJson(username, id, key, JSON.stringify(json), `Update notification subscription${json[1].length > 1 ? 's' : ''}`, (res: any) =>
    console.log(res)
  )*/
}

/**
 * Build a configItem from the operation
 * @param operation an element of 'operations' in generateTemplates
 * @returns
 */
export const configItemFromOperation = (operation: any): { item?: IConfigItem; error?: string } => {
  let conditions: any = {}
  for (const key in operation.template) {
    const task = setConditionFromTemplate(conditions, key, operation.template[key])
    if (task.error) {
      return { error: task.error }
    }
  }

  return { item: { operation: operation.opType, conditions: conditions } }
}

/**
 * Extract and set non-empty conditions from a template item
 * @param conditions
 * @param templateKey
 * @param templateItem
 */
const setConditionFromTemplate = (conditions: any, templateKey: string, templateItem: any): { error?: string } => {
  // If the first key is a condition (e.g. '==') we found the value
  if (isMeaningfulTemplateItem(templateItem)) {
    const option = templateItem.selectedOption
    const value = parseTemplateItemValue(templateItem.type, templateItem.value)

    // Only add valid conditions for non-empty fields
    if (value != undefined) {
      const validation = validateTemplateElement(templateItem, value, templateKey)
      if (validation.success == false) {
        return { error: validation.error }
      }
      conditions[templateKey] = { [option]: value }
    }
    // Recurvise step
  } else if (typeof templateItem == 'object') {
    conditions[templateKey] = {}
    for (const newKey in templateItem) {
      setConditionFromTemplate(conditions[templateKey], newKey, templateItem[newKey])
    }
    // If is empty after checking all fields delete it
    if (Object.keys(conditions[templateKey]).length == 0) {
      delete conditions[templateKey]
    }
  }
  return {}
}

const parseTemplateItemValue = (type: string, value: any) => {
  if (value == undefined) {
    return undefined
  }
  let result: any = value
  if (type == 'array') {
    let sValue = String(value)
    // Remove white spaces between commas
    sValue = sValue.replace(/\s*,\s*/g, ',')
    // Remove trailing comma
    if (sValue[sValue.length - 1] == ',') {
      sValue = sValue.substring(0, sValue.length - 1)
    }
    if (sValue[0] == ',') {
      sValue = sValue.substring(1)
    }
    result = sValue.split(',')
  }

  return result
}

/**
 * Returns whether obj is a meaningful template entry, excluding placeholders (like 'json').
 * By exploiting the fact that 'type' and 'options' are always defined in a template entry,
 * we can weakly infer if the element is indeed a meaningful template entry.
 * @param obj
 * @returns
 */
const isMeaningfulTemplateItem = (obj: any): boolean => {
  if (typeof obj != 'object') {
    return false
  }
  return Object.keys(obj).some(el => ['type', 'options'].indexOf(el) >= 0)
}

/**** MATCHING FUNCTIONS ****/

/**
 * Returns true if matchee has the same key-value pairs as matcher.
 * Matchee can also have different key-value paris not present in matcher.
 * @param matchee object to check
 * @param matcher properties to match
 * @returns
 */
export function matchesProperties(matchee: any, matcher: any): boolean {
  if (matchee === matcher) {
    return true
  }
  if (typeof matchee == 'object' && typeof matcher == 'object') {
    return Object.keys(matcher).every(key => {
      return matchesProperties(matchee[key], matcher[key])
    })
  }
  return false
}

/**
 * Returns true if object a is equal in every key-value pair to object b.
 * @param a object a
 * @param b object b
 * @returns
 */
function matchesExactly(a: any, b: any): boolean {
  if (a === b) {
    return true
  }
  if (typeof a == 'object' && typeof b == 'object') {
    const matKeys = Object.keys(a)
    const propKeys = Object.keys(b)
    return (matKeys.length > propKeys.length ? matKeys : propKeys).every(key => {
      return matchesExactly(a[key], b[key])
    })
  }
  return false
}

/**
 * Returns all the elemets of matcher that match AT LEAST ONE element of matchee
 * following the logic described in the matchingFunction. If the matching function is
 * matchesWithConditions then matcher must be in the form account.config
 * @param matchee matchee, to be matched
 * @param matcher reference
 * @param matchingFunction function handling the matching
 * @param stopAtFirst if true only the first match will be returned
 * @returns
 */
function findByMatching(matchee: any | Array<any>, matcher: any | Array<any>, matchingFunction: Function, invert: boolean = false): Array<any> {
  let limit_matchee = 1
  let limit_matcher = 1
  let result = []

  if (Array.isArray(matchee) && isBlockchainOperation(matchee) == false) {
    limit_matchee = matchee.length
  } else {
    matchee = [matchee]
  }

  if (Array.isArray(matcher) && isBlockchainOperation(matcher) == false) {
    limit_matcher = matcher.length
  } else {
    matcher = [matcher]
  }

  for (let i = 0; i < limit_matchee; i++) {
    for (let j = 0; j < limit_matcher; j++) {
      let isMatch = false
      if (matchingFunction.name == 'matchesWithConditions') {
        const conditions = matcher[j].conditions ? matcher[j].conditions : {}
        //isMatch = matchingFunction(matchee[i], matcher[j].operation, conditions)
        isMatch = matchingFunction(matchee[i], [matcher[j].operation, {}], [{}, conditions])
      } else {
        let ee = matchee[i]
        let er = matcher[j]
        if (invert) {
          let tmp = ee
          ee = er
          er = tmp
        }
        isMatch = matchingFunction(ee, er)
      }
      if (isMatch) {
        result.push(matcher[j])
      }
    }
  }

  return result
}

/**
 * Returns true if the object has the shape of a blockchain operation
 * @param obj
 * @returns
 */
function isBlockchainOperation(obj: any): boolean {
  return Array.isArray(obj) && (obj.length == 2 || obj.length == 3) && typeof obj[0] == 'string' && typeof obj[1] == 'object'
}
