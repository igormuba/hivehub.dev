import { RouteRecordRaw } from 'vue-router'

const name = 'notifications'

const routes: Array<RouteRecordRaw> = [
  {
    path: '/notifications/:account([a-z][a-z0-9-.]{2,15})',
    name: 'notifications',
    component: () => import('~/notifications/pages/Notifications.vue'),
    props: true,
    meta: { title: "@{account}'s notifications" }
  }
]

export const config = {
  name: name,
  enabled: import.meta.env.VITE_APP_ENABLE_NOTIFICATIONS === 'true' ? true : false, // disabled by default
  routes: routes
}
